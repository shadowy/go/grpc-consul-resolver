package consul

import (
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/resolver"
)

type consulResolver struct {
	client     resolver.ClientConn
	service    *Service
	ctx        context.Context
	cancel     context.CancelFunc
	resolveNow chan int
}

func getConsulResolver(client resolver.ClientConn, service *Service) *consulResolver {
	ctx, cancel := context.WithCancel(context.Background())

	res := &consulResolver{
		client:     client,
		service:    service,
		ctx:        ctx,
		cancel:     cancel,
		resolveNow: make(chan int),
	}
	res.start()
	return res
}

func (c *consulResolver) start() {
	go c.watcher()
	c.ResolveNow(resolver.ResolveNowOptions{})
}

func (c *consulResolver) watcher() {
	for {
		select {
		case <-c.resolveNow:
		case <-c.ctx.Done():
			return
		}
		addresses := c.service.Addresses
		if len(addresses) == 0 {
			c.client.ReportError(errors.New("service \"" + c.service.Key + "\" name can't  be resolved"))
		} else {
			c.client.UpdateState(resolver.State{Addresses: addresses})
		}
	}
}

func (c *consulResolver) ResolveNow(o resolver.ResolveNowOptions) {
	logrus.Debug("ResolveNow")
	c.resolveNow <- 1
}

func (c *consulResolver) Close() {
	c.cancel()
}
