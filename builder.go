package consul

import (
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/resolver"
)

type resolverBuilder struct {
	consul   *api.Client
	services *Services
}

const scheme = "consul"

func (b *resolverBuilder) Build(target resolver.Target, connection resolver.ClientConn, _ resolver.BuildOptions) (
	resolver.Resolver, error) {
	f := logrus.Fields{"endpoint": target.Endpoint, "scheme": target.Scheme, "authority": target.Authority}
	logrus.WithFields(f).Debug("resolverBuilder.Build")
	tags, health, err := parseEndpoint(target.Endpoint)
	if err != nil {
		logrus.WithError(err).Error("resolverBuilder.Build parseEndpoint")
		return nil, err
	}
	service := b.services.Add(target.Authority, tags, health)

	return getConsulResolver(connection, service), nil
}

func (*resolverBuilder) Scheme() string {
	return scheme
}
