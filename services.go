package consul

import (
	"context"
	"fmt"
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/resolver"
	"strconv"
	"strings"
	"time"
)

type healthFilter int

const (
	healthFilterUndefined healthFilter = iota
	healthFilterOnlyHealthy
	healthFilterFallbackToUnhealthy
)

const defaultRefreshTime = 20

type Service struct {
	Key       string
	Name      string
	Tags      []string
	Health    healthFilter
	Addresses []resolver.Address
	LastIndex uint64
}

type Services struct {
	health      *api.Health
	list        []*Service
	ctx         context.Context
	opt         *api.QueryOptions
	refreshTime time.Duration
}

var services *Services

func InitServices(consul *api.Client, refreshTime *time.Duration) {
	logrus.Info("consul.InitServices")
	if services != nil {
		return
	}
	services = new(Services)
	services.health = consul.Health()
	services.ctx = context.Background()
	services.opt = (&api.QueryOptions{}).WithContext(services.ctx)
	if refreshTime != nil {
		services.refreshTime = *refreshTime
	} else {
		services.refreshTime = defaultRefreshTime * time.Second
	}
	go services.watch()
}

func (srv *Services) Add(name string, tags []string, health healthFilter) *Service {
	key := srv.buildKey(name, tags, health)
	if res := srv.Find(key); res != nil {
		return res
	}
	service := &Service{
		Key:    key,
		Name:   name,
		Tags:   tags,
		Health: health,
	}
	srv.proceed(service)
	srv.list = append(srv.list, service)
	return service
}

func (srv *Services) Find(key string) *Service {
	for i := range srv.list {
		res := srv.list[i]
		if res.Key == key {
			return res
		}
	}
	return nil
}

func (srv *Services) Clear() {
	srv.list = nil
}

func (srv *Services) watch() {
	for {
		for i := range srv.list {
			srv.proceed(srv.list[i])
		}
		time.Sleep(srv.refreshTime)
	}
}

func (srv Services) proceed(service *Service) {
	entries, meta, err := srv.health.ServiceMultipleTags(service.Name, service.Tags, service.Health == healthFilterOnlyHealthy, srv.opt)
	if err != nil {
		logrus.WithFields(logrus.Fields{"key": service.Key}).WithError(err).Error("consul.Services.proceed query")
	}
	if meta.LastIndex == service.LastIndex {
		return
	}
	if service.Health == healthFilterOnlyHealthy {
		pos := 0
		for i := range entries {
			entry := entries[i]
			if entry.Checks.AggregatedStatus() == api.HealthPassing {
				entries[pos] = entries[i]
				pos++
			}
		}
		entries = entries[:pos]
	}

	logrus.WithField("lastIndex", meta.LastIndex).Debug("consul.Services.proceed query")
	addresses := make([]resolver.Address, 0, len(entries))
	for i := range entries {
		entry := entries[i]
		address := entry.Service.Address
		if address == "" {
			address = entry.Node.Address
			logrus.WithField("key", service.Key).Warn("Use node address instead service address")
		}
		addresses = append(addresses, resolver.Address{Addr: fmt.Sprintf("%s:%d", address, entry.Service.Port)})
	}
	service.Addresses = addresses
	service.LastIndex = meta.LastIndex
}

func (srv Services) buildKey(name string, tags []string, health healthFilter) string {
	res := strings.ToLower(name) + "["
	for i, tag := range tags {
		if i > 0 {
			res += ","
		}
		res += strings.ToLower(tag)
	}
	res += "]-" + strconv.Itoa(int(health))
	return res
}
