module gitlab.com/shadowy/go/grpc-consul-resolver

go 1.16

require (
	github.com/golang/protobuf v1.5.1 // indirect
	github.com/hashicorp/consul v1.9.4 // indirect
	github.com/hashicorp/consul/api v1.8.1 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	google.golang.org/grpc v1.36.0 // indirect
)
