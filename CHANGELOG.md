# CHANGELOG

<!--- next entry here -->

## 0.1.0
2021-03-18

### Features

- consul resolver (0b58d38759918e9f3d119ca169005592ab201b5f)

### Fixes

- fix lint issue (2564882d0a63d5052d4aeb7a4708880009fd1641)

## 0.1.1
2020-12-01

### Fixes

- rename namespace (7b635e26d46eba9ea32b614d8d3ca957b427b472)

## 0.1.0
2020-12-01

### Features

- start implementation (de733a0743857933431baf09615b735a0a73a8a0)