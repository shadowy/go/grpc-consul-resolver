package consul

import (
	"github.com/hashicorp/consul/api"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc/resolver"
	"time"
)

func InitConsulResolver(consul *api.Client, refreshTime *time.Duration) {
	InitServices(consul, refreshTime)
	resolver.Register(&resolverBuilder{consul: consul, services: services})
}

func ResetResolver() {
	logrus.Info("consulResolver.ResetResolver")
	services.Clear()
}
