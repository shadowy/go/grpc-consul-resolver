package consul

import (
	"errors"
	"fmt"
	"strings"
)

func splitOpts(opts string) ([]string, error) {
	const maxParams = 3

	spl := strings.Split(opts, "&")
	if len(spl) <= maxParams {
		return spl, nil
	}

	return nil, fmt.Errorf("endpoint can only contain <=%d parameters", maxParams)
}

func splitOptKV(opt string) (key, value string, err error) {
	const parts = 2
	spl := strings.Split(opt, "=")
	if len(spl) == parts {
		return spl[0], spl[1], nil
	}

	return "", "", errors.New("parameter must contain a single '='")
}

func extractOpts(opts string) (tags []string, health healthFilter, err error) {
	health = healthFilterOnlyHealthy
	optsSl, err := splitOpts(opts)
	if err != nil {
		return nil, health, err
	}

	for _, opt := range optsSl {
		key, value, errKV := splitOptKV(opt)
		if errKV != nil {
			return nil, healthFilterUndefined, errKV
		}

		switch key = strings.ToLower(key); key {
		case "tags":
			tags = strings.Split(value, ",")

		case "health":
			switch strings.ToLower(value) {
			case "healthy":
				health = healthFilterOnlyHealthy
			case "fallbacktounhealthy":
				health = healthFilterFallbackToUnhealthy
			default:
				return nil, health, fmt.Errorf("unsupported health parameter value: '%s'", value)
			}

		default:
			return nil, health, fmt.Errorf("unsupported parameter: '%s'", key)
		}
	}

	return tags, health, err
}

func parseEndpoint(opts string) (tags []string, health healthFilter, err error) {
	if opts == "" {
		return nil, healthFilterOnlyHealthy, nil
	}
	return extractOpts(opts)
}
