all: test lint-full

test:
	@echo ">> test"
	#@go test ./... -coverprofile cover.out
	#@go tool cover -html=cover.out -o cover.html

lint-full: lint card

card:
	@echo ">> card"
	@goreportcard-cli -v

lint:
	@echo ">> lint"
	@golangci-lint run
